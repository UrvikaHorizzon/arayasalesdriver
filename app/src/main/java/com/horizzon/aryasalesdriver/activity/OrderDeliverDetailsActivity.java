package com.horizzon.aryasalesdriver.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.horizzon.aryasalesdriver.Downloader;
import com.horizzon.aryasalesdriver.R;
import com.horizzon.aryasalesdriver.model.PendingOrderItem;
import com.horizzon.aryasalesdriver.model.Productinfo;
import com.horizzon.aryasalesdriver.model.User;
import com.horizzon.aryasalesdriver.retrofit.APIClient;
import com.horizzon.aryasalesdriver.utils.CustPrograssbar;
import com.horizzon.aryasalesdriver.utils.SessionManager;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.horizzon.aryasalesdriver.retrofit.APIClient.Base_URL;
import static com.horizzon.aryasalesdriver.utils.SessionManager.CURRUNCY;

public class OrderDeliverDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.txt_orderid)
    TextView txtOrderid;
    @BindView(R.id.txt_total)
    TextView txtTotal;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.txt_qty)
    TextView txtQty;
    @BindView(R.id.txt_address)
    TextView txtAddress;
    @BindView(R.id.txt_email)
    TextView txtEmail;
    @BindView(R.id.txt_paymode)
    TextView txtPaymode;
    @BindView(R.id.txt_name)
    TextView txtName;
    @BindView(R.id.lvl_items)
    LinearLayout lvlItems;

    @BindView(R.id.img_sing)
    ImageView img_sing;
    @BindView(R.id.btnInvoice)
    Button btnInvoice;

    ArrayList<Productinfo> productinfoArrayList;
    PendingOrderItem order;
    SessionManager sessionManager;

    CustPrograssbar custPrograssbar;
    User user;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_deliver_details);
        ButterKnife.bind(this);
        btnInvoice.setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Order Delivered Details");
        getSupportActionBar().setElevation(0f);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails("");
        custPrograssbar = new CustPrograssbar();
        order = (PendingOrderItem) getIntent().getParcelableExtra("MyClass");
        productinfoArrayList = getIntent().getParcelableArrayListExtra("MyList");

        txtOrderid.setText("" + order.getOrderid());
        txtTotal.setText(sessionManager.getStringData(CURRUNCY) + " " + order.getTotal());
        txtTime.setText("" + order.getTimesloat());
        txtOrderid.setText("" + order.getOrderid());
        txtQty.setText("" + productinfoArrayList.size());
        txtAddress.setText("" + order.getDelivery());
        txtEmail.setText("" + order.getEmail());
        txtPaymode.setText("" + order.getPMethod());
        txtName.setText("" + order.getName());
        byte[] decodedString;

        if (order.getSign() != null) {
            decodedString = Base64.decode(order.getSign(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            img_sing.setImageBitmap(decodedByte);
        }

        setJoinPlayrList(lvlItems, productinfoArrayList);
    }

    private void setJoinPlayrList(LinearLayout lnrView, ArrayList<Productinfo> productinfoArrayList) {

        lnrView.removeAllViews();

        for (int i = 0; i < productinfoArrayList.size(); i++) {
            Productinfo listdatum = productinfoArrayList.get(i);
            LayoutInflater inflater = LayoutInflater.from(OrderDeliverDetailsActivity.this);

            View view = inflater.inflate(R.layout.pending_order_item, null);
            ImageView imgView = view.findViewById(R.id.imageView);
            TextView txtTitle = view.findViewById(R.id.txt_title);
            TextView txtItems = view.findViewById(R.id.txt_items);
            TextView txt_wight = view.findViewById(R.id.txt_wight);
            TextView txt_price = view.findViewById(R.id.txt_price);
            TextView txt_pricedis = view.findViewById(R.id.txt_pricedis);
            Glide.with(OrderDeliverDetailsActivity.this).load(Base_URL + listdatum.getProductImage()).placeholder(R.drawable.slider).into(imgView);
            txtTitle.setText("" + listdatum.getProductName());
            txtItems.setText(" X " + listdatum.getProductQty() + " Items");
            txt_wight.setText(" " + listdatum.getProductWeight() + "");
            txt_price.setText(sessionManager.getStringData(CURRUNCY) + listdatum.getProductPrice() + "");
            if (Double.parseDouble(listdatum.getDiscount()) == 0) {
                txt_pricedis.setVisibility(View.GONE);
            } else {
                txt_pricedis.setPaintFlags(txt_pricedis.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                txt_pricedis.setText(sessionManager.getStringData(CURRUNCY) + "" + listdatum.getDiscount());
            }

            lnrView.addView(view);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnInvoice:
                if(order.getRid() != null){

                        String pfdFilePath = Base_URL + order.getRid();
                        String shot = order.getRid();
                        String[] separated = shot.split("/");

                        String path = separated[1];
                        Log.d("TAG", "setJoinPlayrList: " + path);

                        new Downloader().execute(pfdFilePath, path);
                        showPdf(path, pfdFilePath);

                }else {
                    Toast.makeText(this, "No data.", Toast.LENGTH_SHORT).show();
                }

                break;
            default:
        }
    }

    public void showPdf(String paths, String pfdFilePath) {

        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/testthreepdf/" + paths);  // -> filename = maven.pdf
        Uri path = Uri.fromFile(pdfFile);
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(pfdFilePath)));
        } catch (ActivityNotFoundException e) {
            Log.d("TAG", "view: " + e.getMessage());
        }
    }
}
